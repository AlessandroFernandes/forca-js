
createSprite = (act) => {

    let $el = $(act);

    const frames = ['frame1', 'frame2', 'frame3', 'frame4', 'frame5',
                    'frame6', 'frame7', 'frame8', 'frame9'];

    let current = 0;
    let last = frames.length - 1;

   

    $el.addClass(frames[current]);

    selectFrame = (inFrame, toFrame) => {
        $el.removeClass(inFrame)
           .addClass(toFrame)
    }

    hasNext = () => {
        return current + 1 <= last;
    }

    nextFrame = () => { 
        if(hasNext()) {selectFrame(frames[current], frames[++current])};
    }
   
    reset =() => {
            selectFrame(frames[current], frames[0]);
            current = 0
    }


    isFinished = () => {
        return !hasNext();
    }

    return {
        isFinished : isFinished,
        nextFrame: nextFrame,
        reset: reset
    }

}

