criaController = jogo => {

    let $entrada = $('#entrada');
    let $lacunas = $('.lacunas');

    let exibeLacunas = function () {
    
        $lacunas.empty();
        jogo.getlacunas().forEach(lacuna => {
            $('<li>').addClass('lacuna')
                     .text(lacuna)
                     .appendTo($lacunas)
        });

    };

    mudaPlaceHolder = texto => {

        $entrada.attr('placeholder', texto)
    }

    limparCampo = () => {
        
        $entrada.val('')
    };


    guardaPalavraSecreta = () => {
            try{
                jogo.setPalavraSecreta($entrada.val().trim());
                $entrada.val('');
                mudaPlaceHolder('Digite seu Chute');
                exibeLacunas();
            }catch(erro){
                alert(erro.message)
            }   
    };

    fimDeJogo = () =>{
        return jogo.ganhou() ? "Você Acertou" : "Você Perdeu";
    }

    resetGame = () => {
        alert(fimDeJogo());
        jogo.reiniciar();
        $lacunas.empty();
        mudaPlaceHolder('Palavra Secreta');
    }

    lerChute = () => {
            try{
                jogo.chuteJogador($entrada.val().trim().substr(0,1));
                limparCampo();
                exibeLacunas();

                if(jogo.ganhouOuPerdeu()){
                    setTimeout(() => {
                        resetGame();
                    }, 200);
                
                }
            }catch(erro){
                alert(erro.message)
            }
    }
    
    inicia = () => {

        $entrada.keypress(function (event) {
            if (event.which == 13) {
                switch (jogo.getEtapa()) {
                    case 1:
                        guardaPalavraSecreta();
                        break;
                    case 2:
                        lerChute();
                        break;
                }
            }
        });
    }

    return { inicia: inicia };
};