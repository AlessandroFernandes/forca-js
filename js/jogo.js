criaJogo = (sprite) => {

    let palavraSecreta = '';
    let etapa = 1;
    let lacuna = [];
    // let ganhouGame = false;
    // let perdeuGame = false;

    ganhou = () => {
        return lacuna.length
        ? !lacuna.some(teste => {
          return teste == ""; 
        })
        : false
    }

    perdeu = () => {
        return sprite.isFinished();
    }
   
    ganhouOuPerdeu = () => {

        return ganhou()|| perdeu();
    }

    reiniciar = () => {
        sprite.reset();
        lacuna = [];
        etapa = 1;
    }
    
    chuteJogador = chute => {

        if(!chute.trim()) throw Error("Chute inválido!")
    
        let testador = new RegExp(chute, 'gi'),
        contem = false;

        while(condicao = testador.exec(palavraSecreta)){
            lacuna[condicao.index] = chute;
            contem = true;
        }

        if(!contem){
            sprite.nextFrame();
        }

    }

    setPalavraSecreta = palavra =>{
       
    if(!palavra.trim()) throw Error("Palavra Secreta inválida!");

       palavraSecreta = palavra;
       etapa = 2;
       setLacuna();
       
    }

    setLacuna = () => {
        lacuna = Array(palavraSecreta.length).fill('');
    }

    getlacunas = () => {
    return lacuna;
    }

    getEtapa = () => {
        return etapa;
    }

    return {
        setPalavraSecreta: setPalavraSecreta,
        getlacunas: getlacunas,
        getEtapa: getEtapa,
        chuteJogador: chuteJogador,
        ganhou: ganhou,
        perdeu: perdeu,
        ganhouOuPerdeu: ganhouOuPerdeu,
        reiniciar: reiniciar
    }
}